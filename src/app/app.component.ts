import { faExternalLinkAlt, faEllipsisV, faCog} from '@fortawesome/free-solid-svg-icons';
import { faGitlab, faFacebook, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FaIconLibrary, } from '@fortawesome/angular-fontawesome';
import { Component } from '@angular/core';
import { APPS, Tile } from './data';
import { Observable } from 'rxjs';
import { ThemeService } from './core/services/theme.service';

import {OverlayContainer} from '@angular/cdk/overlay';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  apps: Tile[];
  mobile = true;
  //theme: Observable<string>;
  overlayContainer: OverlayContainer;

  theme: string;


  constructor(private themeService: ThemeService, library: FaIconLibrary, overlayContainer: OverlayContainer) {
    library.addIcons(faGitlab, faFacebook, faInstagram, faExternalLinkAlt, faEllipsisV, faCog),
    this.overlayContainer = overlayContainer;
    //library.add(faGitlab, faFacebook, faInstagram, faExternalLinkAlt, faEllipsisV);
    this.apps = [];
    APPS.forEach((app: Tile) => this.apps.push(app));
  }

  ngOnInit() {
   this.overlayContainer.getContainerElement().classList.add('website-default-theme');
    this.themeService.themeObs.pipe().subscribe(theme => {
      this.overlayContainer.getContainerElement().classList.replace(this.theme, theme);
      this.theme = theme;
    });
    if (window.screen.width > 400) { // 768px portrait
      this.mobile = false;
    }
  }

  // DEFAULT-THEME BLACK-THEME
  onThemeSelect(checked: boolean) {
    if (checked) {
      this.themeService.setTheme('website-dark-theme');
      // console.log("on");
    } else {
      this.themeService.setTheme('website-default-theme');
      // console.log("off");
    }
  }
}
