import { Tile } from './tile';
import { Privacy } from './privacy';

// export const APPS_PRIVACY: Privacy[] = [
//     {
//         title: 'string',
//         general: '',
//         information: 'string',
//         log: 'string',
//         cookies: 'string',
//         providers: 'string',
//         security: 'string',
//         link: 'string',
//         children: 'string',
//         changes: 'string',
//         contact: 'string',
//     },
//     {
//         title: 'string',
//         general: '',
//         information: 'string',
//         log: 'string',
//         cookies: 'string',
//         providers: 'string',
//         security: 'string',
//         link: 'string',
//         children: 'string',
//         changes: 'string',
//         contact: 'string',
//     },
//     {
//         title: 'FixiCalc',
//         general: 
//         'Jack Meyer built the FixiCalc app as a Free app. This SERVICE is provided by Jack Meyer at no cost and is intended for use as is. ' + 
//         'This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.' +
//         'If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. ' +
//         'The Personal Information that I collect is used for providing and improving the Service. ' +
//         'I will not use or share your information with anyone except as described in this Privacy Policy. ' +
//         'The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at FixiCalc unless otherwise defined in this Privacy Policy.' ,
//         information: 
//         'For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information, ' + 
//         'including but not limited to Luca Meyer. The information that I request will be retained on your device and is not collected by me in any way. ' +
//         'The app does use third party services that may collect information used to identify you. ' +
//         'Link to privacy policy of third party service providers used by the app',
//         links: [['Google Play Services', 'https://www.google.com/policies/privacy/']],
//         log: 'string',
//         cookies: 'string',
//         providers: 'string',
//         security: 'string',
//         link: 'string',
//         children: 'string',
//         changes: 'string',
//         contact: 'string',
//     },
//     {
//         title: 'string',
//         general: '',
//         information: 'string',
//         log: 'string',
//         cookies: 'string',
//         providers: 'string',
//         security: 'string',
//         link: 'string',
//         children: 'string',
//         changes: 'string',
//         contact: 'string',
//     },
//     {
//         title: 'string',
//         general: '',
//         information: 'string',
//         log: 'string',
//         cookies: 'string',
//         providers: 'string',
//         security: 'string',
//         link: 'string',
//         children: 'string',
//         changes: 'string',
//         contact: 'string',
//     },
//     {
//         title: 'string',
//         general: '',
//         information: 'string',
//         log: 'string',
//         cookies: 'string',
//         providers: 'string',
//         security: 'string',
//         link: 'string',
//         children: 'string',
//         changes: 'string',
//         contact: 'string',
//     },
// ]

export const APPS: Tile[] = [
    {
        id: 0, cols: 1, rows: 1, title: 'TSV Waldenbuch',
        images: ['tsvAndroid.png'],
        androidLink: 'https://play.google.com/store/apps/details?id=de.jboka.tsvwaldenbuch',
        iosLink: 'https://itunes.apple.com/us/app/tsv-waldenbuch/id1348882088?l=de&ls=1&mt=8',
        infoText: 'Lesen Sie die neusten Berichte und bekommen Sie einen Überblick über die aktuellen Termine des lokalen TSV Waldenbuch. Berichte sowie Events können geteilt werden. Zudem können Sie Events zu Ihrem Kaldender hinzufügen und sich die Event-Location auf einer Karte anzeigen lassen. Die App ist inoffiziell.',
        privacy: ['free', 'at no cost'],
        privacyLinks: [['Google Play Services', 'https://www.google.com/policies/privacy/']],
    },
    {
        id: 1, cols: 1, rows: 1, title: 'Ponde Check I/O',
        images: ['ponde.png'],
        androidLink: '#',
        iosLink: '#',
        infoText: 'Diese App ist eine Realisierung für ein Check-In-Out-System, bei dem mittels QR-Code oder manuell Personen ein/ausgecheckt und personenbezogene Daten angezeigt werden können. Alle Daten werden in Echtzeit auf allen Endgeräten synchronisiert',
        privacy: ['free', 'at no cost'],
        privacyLinks: [['Google Play Services', 'https://www.google.com/policies/privacy/']],
    },
    {
        id: 2, cols: 1, rows: 1, title: 'FixiCalc',
        images: ['fixedGear.png'],
        androidLink: 'https://play.google.com/store/apps/details?id=de.seven4one.fixedgearcalculator',
        iosLink: 'https://apps.apple.com/de/app/fixed-gear-calculator/id1378226423?l=de&ls=1',
        infoText: 'Genervt von Websites, Excel oder dem guten alten Taschenrecher um die beste Übersetzung /Kadenz herauszufinden für das nächste fixed Rennen oder Ausfahrt. Dann ist dies für dich das Richtige',
        privacy: ['ad-supported', 'at no cost'],
        privacyLinks: 
        [['Google Play Services', 'https://www.google.com/policies/privacy/'], 
        ['AdMob' ,'https://support.google.com/admob/answer/6128543?hl=en']],
    },
    {
        id: 3, cols: 1, rows: 1, title: 'Work Time Tracker',
        images: ['worktimetracker.png'],
        androidLink: '#',
        iosLink: '#',
        infoText: 'WorkTimeTracker ist eine einfache Arbeitszeit-Erfassungs-App. Für jeden angelegten Job kann man Sessions starten oder manuell anlegen, um die tägliche Arbeitszeit korrekt zu erfassen und schlussendlich effektiver zu arbeiten',
        privacy: ['free', 'at no cost'],
        privacyLinks: [['Google Play Services', 'https://www.google.com/policies/privacy/']],
    },
    {
        id: 4, cols: 1, rows: 1, title: 'All Time Table',
        images: ['seven4one.png'],
        androidLink: '#',
        iosLink: '#',
        infoText: 'Interesiest Du dich auf welchem Platz dein Liebelingsverein in der Ewigen Tabelle verschiedenster Ligen ist? Mit AllTimeTables kannst Du die Ewigen Tabellen verschiedener Ligen und Meisterschaften abrufen.',
        privacy: ['free', 'at no cost'],
        privacyLinks: [['Google Play Services', 'https://www.google.com/policies/privacy/']],
    },
];

