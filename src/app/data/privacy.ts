export interface Privacy {
    title: string;
    general: string;
    information: string;
    //links: any;
    links: any[];
    log: string;
    cookies: string;
    providers: string;
    security: string;
    link: string;
    children: string;
    changes: string;
    contact: string;
  }