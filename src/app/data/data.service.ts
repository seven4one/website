import { Injectable } from '@angular/core';
import { Tile, APPS } from '.';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  getApps(): Observable<Tile[]> {
    return of(APPS);
  }

  getApp(id: number): Observable<Tile> {
    // TODO: send the message _after_ fetching the hero
    return of(APPS.find(app => app.id == id));
  }

}
