import { Privacy } from './privacy';

export interface Tile {
    id: number;
    cols: number;
    rows: number;
    title: string;
    infoHidden?: true;
    images?: [string];
    androidLink: string;
    iosLink: string;
    infoText: string;
    // [open source | free | freemium | ad-supported | commercial]
    privacy: string[];
    privacyLinks: string[][];
  }