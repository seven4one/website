import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page/main-page.component';
import { DetailPageComponent } from './detail-page/detail-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';


//import { MatGridListModule } from '@angular/material/grid-list';

import { MatGridListModule, MatToolbarModule, MatButtonModule, MatMenuModule, 
  MatTabsModule, MatCardModule, MatListModule, MatSidenavModule, MatIconModule,
MatSlideToggleModule, MatButtonToggleModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { WePageComponent } from './we-page/we-page.component';
import { CoreModule } from './core/core.module';
import { ThemeService } from './core/services/theme.service';

@NgModule({
  declarations: [
    AppComponent,
    DetailPageComponent,
    MainPageComponent,
    ContactPageComponent,
    WePageComponent,
  ],
  imports: [
    FontAwesomeModule,
    BrowserModule,
    AppRoutingModule,
    MatGridListModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatTabsModule,
    MatListModule,
    MatCardModule,
    MatSidenavModule,
    MatIconModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    BrowserAnimationsModule,
    CoreModule,
  ],
  providers: [ThemeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
