import { Component, OnInit } from '@angular/core';
import { Tile, APPS } from '../data';
import { DataService } from '../data/data.service';


@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  tiles: Tile[];
  cols: number;
  rowHeight: number;

  constructor(private dataServce: DataService) {}

  ngOnInit() {
    this.tiles;
    this.rowHeight = 300;
    this.dataServce.getApps().subscribe((tiles: Tile[]) => this.tiles = tiles)
    let cols = Math.floor(window.innerWidth / this.rowHeight)
    this.cols = cols < this.tiles.length ? cols : this.tiles.length;
  }

  onResize(event) {
    //console.log(Math.floor(event.target.innerWidth / this.rowHeight));
    let cols = Math.floor(event.target.innerWidth / this.rowHeight);
    this.cols = cols < this.tiles.length ? cols : this.tiles.length;
  }

  getURL(tile: Tile) {
    var userAgent = window.navigator.userAgent,
      platform = window.navigator.platform,
      macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
      windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
      iosPlatforms = ['iPhone', 'iPad', 'iPod'],
      os = null;

    if (/Android/.test(userAgent)) {
      return tile.androidLink;
    } else {
      return tile.iosLink != null ? tile.iosLink : tile.androidLink;
    }
  }
}
