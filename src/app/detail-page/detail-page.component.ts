import { Component, OnInit } from '@angular/core';
import { Tile } from '../data';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data/data.service';
import { faExternalLinkAlt, faEllipsisV, faCog} from '@fortawesome/free-solid-svg-icons';
import { faAndroid, faAppStoreIos } from '@fortawesome/free-brands-svg-icons';
import { FaIconLibrary, } from '@fortawesome/angular-fontawesome';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss']
})
export class DetailPageComponent implements OnInit {
  app: Tile;
  selectIndex: number;
  cols: number;
  rowHeight: number;

  constructor(private activeRoute: ActivatedRoute, private dataServce: DataService, private library: FaIconLibrary) {
    library.addIcons(faAndroid, faAppStoreIos);
   }

  ngOnInit() {
    this.selectIndex = 0;
    this.rowHeight = 300;
    this.activeRoute.params.subscribe(routeParams => {
      if (String(routeParams.id).includes('privacy')) {
        this.selectIndex = 1;
        this.dataServce.getApp(+String(routeParams.id).substring(0, 1)).subscribe((app: Tile) => {
          this.app = app
        });
      } else {
        this.dataServce.getApp(routeParams.id).subscribe((app: Tile) => {
          this.app = app
        });
      }

    });
    let cols = Math.floor(window.innerWidth / this.rowHeight)
    this.cols = cols < this.app.images.length ? cols : this.app.images.length;
  }

  onResize(event) {
    //console.log(Math.floor(event.target.innerWidth / this.rowHeight));
    let cols = Math.floor(event.target.innerWidth / this.rowHeight);
    this.cols = cols < this.app.images.length ? cols : this.app.images.length;
  }


}
