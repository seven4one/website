import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class ThemeService {
  private _theme = new Subject<string>();
  themeObs = this._theme.asObservable();

  setTheme(theme: string): void {
    this._theme.next(theme);
  }
}
