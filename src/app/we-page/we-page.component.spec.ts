import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WePageComponent } from './we-page.component';

describe('WePageComponent', () => {
  let component: WePageComponent;
  let fixture: ComponentFixture<WePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
