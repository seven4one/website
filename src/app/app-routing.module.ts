import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DetailPageComponent } from './detail-page/detail-page.component';
import { MainPageComponent } from './main-page/main-page.component'
import { ContactPageComponent } from './contact-page/contact-page.component';
import { WePageComponent} from './we-page/we-page.component'
import { CommonModule } from '@angular/common';


const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full' },
  { path: 'main', component: MainPageComponent },
  { path: 'appcomponent', component: AppComponent },
  { path: 'detail/:id', component: DetailPageComponent },
  { path: 'we', component: WePageComponent },
  { path: 'contact', component: ContactPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
